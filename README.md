# Gender Classification

### Chương trình phát hiện khuôn mặt nam nữ trên webcam

Trước tiên, cần cài đặt các thư viện cần thiết, dùng câu lệnh
`pip install -r requirements.txt`

Để khởi động chương trình, chạy file `run_classifier.py`

Để thực hiện tự train lại model, có thể tải dataset [tại đây](https://drive.google.com/drive/folders/1KN3gxOJ4dkbfHeiwy4jfAmAiVphXhIpG?usp=sharing
) (thư mục gender_dataset_face) và đặt vào thư mục project (nơi chứa file train.py)
, sau đó chạy file `train.py`. 
